# UD01-T01 Recolección de evidencias volátiles Windows 10

## Máquina virtual necesaria

- `XW10-01 analisis forense.ova` (`usuario` -> `abc123.`)
- En el instituto: disponible en `R:\MVirtuais\ciberCE\AnaliseFI\Win10-ElementosVolatiles.ova`
- Enlaces de descarga desde el exterior:
- [Google Drive](https://drive.google.com/file/d/14IgA4dMEu8WAuEVRJ7jUZ4bGvNogNv8P/view)

## Enunciado

Accede a la máquina `Windows 10` suministrada con el usuario: `usuario` -> `abc123.`. y, utilizando los comandos y herramientas vistos en los apuntes, trata de responder a las siguientes preguntas:

- ¿Qué usuarios y grupos existen en el equipo?
    **Usuarios:**

    ```batch
    net user > UsuariosEquipo.log

    Cuentas de usuario de \\XW10-XX

    -------------------------------------------------------------------------------
    Administrador            DefaultAccount           Invitado
    usuario                  UsuarioAlgoMalvado       UsuarioMalvado
    WDAGUtilityAccount
    Se ha completado el comando correctamente.
    ```

    **Grupos:**

    ```batch
    net localgroup > GruposEquipo.log

    Alias para \\XW10-XX

    -------------------------------------------------------------------------------
    *Administradores
    *Administradores de Hyper-V
    *Duplicadores
    *GrupoMalvado
    *GrupoParaMaldades
    *IIS_IUSRS
    *Invitados
    *Lectores del registro de eventos
    *Operadores criptogr�ficos
    *Operadores de asistencia de control de acceso
    *Operadores de configuraci�n de red
    *Operadores de copia de seguridad
    *Propietarios del dispositivo
    *System Managed Accounts Group
    *Usuarios
    *Usuarios avanzados
    *Usuarios COM distribuidos
    *Usuarios de administraci�n remota
    *Usuarios de escritorio remoto
    *Usuarios del monitor de sistema
    *Usuarios del registro de rendimiento
    Se ha completado el comando correctamente.
    ```

- ¿Ha realizado el usuario alguna navegación por Internet “sospechosa”?¿Con qué navegador?
    Buscouse información sobre como crackear contrasinais de windows e tutoriais de como explotar exploits. Empregou o `Firefox`.

    Primerio analicei o resultado do software `BrowsingHistoryView.exe` para ver se había algunha actividade sospeitosa e despois revisei os logs por separado de cada navegador.

    **Para ver tódolos navegadores:**

    ```log
    .\Ferramentas\NirSoft\BrowsingHistoryView.exe /VisitTimeFilterType 1 /HistorySource 2 /LoadIE 1 /LoadFirefox 1/LoadChrome 1 /LoadSafari 1 /stab Historial.log

    [...]

    URL	Title	Visit Time	Visit Count	Visited From	Visit Type	Web Browser	User Profile	Browser Profile	URL Length	Typed Count	History File	Record ID
    http://www.oxid.it/ca_um/	oxid.it	13/06/2021 13:28:04	1	https://www.fullaprendizaje.com/2016/08/Las-10-mejores-herramientas-para-descifrar-contrasenas-en-Windows-Linux-y-aplicaciones-Web..html	Link	Firefox	usuario	13487k28.default-release	25		C:\Users\usuario\AppData\Roaming\Mozilla\Firefox\Profiles\13487k28.default-release\places.sqlite	6
    https://codeload.github.com/vanhauser-thc/thc-hydra/zip/refs/heads/master	thc-hydra-master.zip	13/06/2021 13:29:25	0		Download	Firefox	usuario	13487k28.default-release	73		C:\Users\usuario\AppData\Roaming\Mozilla\Firefox\Profiles\13487k28.default-release\places.sqlite	17
    https://consent.youtube.com/m?continue=https%3A%2F%2Fwww.youtube.com%2Fwatch%3Fv%3DHGFhJado4c8&gl=ES&m=0&pc=yt&uxe=23983172&hl=es&src=1		13/06/2021 13:33:36	1			Internet Explorer 10/11 / Edge	usuario		135		C:\Users\usuario\AppData\Local\Microsoft\Windows\WebCache\WebCacheV01.dat	20
    https://consent.youtube.com/m?continue=https%3A%2F%2Fwww.youtube.com%2Fwatch%3Fv%3DHGFhJado4c8&gl=ES&m=0&pc=yt&uxe=23983172&hl=es&src=1		13/06/2021 13:33:35	2			Internet Explorer 10/11 / Edge	usuario		135		C:\Users\usuario\AppData\Local\Microsoft\Windows\WebCache\WebCacheV01.dat	36
    https://github.com/vanhauser-thc/thc-hydra	GitHub - vanhauser-thc/thc-hydra: hydra	13/06/2021 13:28:57	1	https://www.redeszone.net/2019/06/08/hydra-9-0-herramienta-romper-contrasenas/	Link	Firefox	usuario	13487k28.default-release	42		C:\Users\usuario\AppData\Roaming\Mozilla\Firefox\Profiles\13487k28.default-release\places.sqlite	14
    https://github.com/vanhauser-thc/thc-hydra/archive/refs/heads/master.zip		13/06/2021 13:29:20	1		Link	Firefox	usuario	13487k28.default-release	72		C:\Users\usuario\AppData\Roaming\Mozilla\Firefox\Profiles\13487k28.default-release\places.sqlite	16
    https://github.com/vanhauser-thc/thc-hydra/releases/tag/v9.2	Release hydra v9.2 · vanhauser-thc/thc-hydra · GitHub	13/06/2021 13:29:03	1	https://github.com/vanhauser-thc/thc-hydra	Link	Firefox	usuario	13487k28.default-release	60		C:\Users\usuario\AppData\Roaming\Mozilla\Firefox\Profiles\13487k28.default-release\places.sqlite	15
    https://jztkft.dl.sourceforge.net/project/ophcrack/ophcrack/3.8.0/ophcrack-3.8.0-bin.zip	ophcrack-3.8.0-bin.zip	13/06/2021 13:29:53	0		Download	Firefox	usuario	13487k28.default-release	88		C:\Users\usuario\AppData\Roaming\Mozilla\Firefox\Profiles\13487k28.default-release\places.sqlite	23
    https://katie.v4.omgtnc.com/api/user/01baf62baecc807cff196e6722da82538fd071ada4.r?tk=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJwdWIiOiI1MDVjNmI4MTcxMzIwNDAyNTc1YjFkNmUiLCJ0cyI6IjA2MTMxMTI4IiwiZCI6Im94aWQuaXQifQ.2RbPmSqsPeb-yTry9wUvoC3YW5zgZ2lJXS6ej3E6zj8		13/06/2021 13:28:06	1	http://www.oxid.it/ca_um/	Link	Firefox	usuario	13487k28.default-release	252		C:\Users\usuario\AppData\Roaming\Mozilla\Firefox\Profiles\13487k28.default-release\places.sqlite	7
    https://ophcrack.sourceforge.io/	Ophcrack	13/06/2021 13:29:35	1		Link	Firefox	usuario	13487k28.default-release	32		C:\Users\usuario\AppData\Roaming\Mozilla\Firefox\Profiles\13487k28.default-release\places.sqlite	19
    https://ophcrack.sourceforge.io/download.php?type=ophcrack	Ophcrack	13/06/2021 13:29:38	1	https://ophcrack.sourceforge.io/	Link	Firefox	usuario	13487k28.default-release	58		C:\Users\usuario\AppData\Roaming\Mozilla\Firefox\Profiles\13487k28.default-release\places.sqlite	20
    https://sourceforge.net/projects/ophcrack/files/ophcrack/3.8.0/ophcrack-3.8.0-bin.zip/download	Download ophcrack from SourceForge.net	13/06/2021 13:29:42	1	https://ophcrack.sourceforge.io/download.php?type=ophcrack	Link	Firefox	usuario	13487k28.default-release	94		C:\Users\usuario\AppData\Roaming\Mozilla\Firefox\Profiles\13487k28.default-release\places.sqlite	21
    https://sourceforge.net/projects/ophcrack/postdownload	Find out more about ophcrack | SourceForge.net	13/06/2021 13:30:30	1		Link	Firefox	usuario	13487k28.default-release	54		C:\Users\usuario\AppData\Roaming\Mozilla\Firefox\Profiles\13487k28.default-release\places.sqlite	24
    https://wiki.elhacker.net/		13/06/2021 13:33:51	1			Internet Explorer 10/11 / Edge	usuario		26		C:\Users\usuario\AppData\Local\Microsoft\Windows\WebCache\WebCacheV01.dat	41
    https://wiki.elhacker.net/bugs-y-exploits/introduccion/interpretar-y-compilar-exploits		13/06/2021 13:33:51	2			Internet Explorer 10/11 / Edge	usuario		86		C:\Users\usuario\AppData\Local\Microsoft\Windows\WebCache\WebCacheV01.dat	40
    https://wiki.elhacker.net/bugs-y-exploits/introduccion/interpretar-y-compilar-exploits		13/06/2021 13:33:59	1			Internet Explorer 10/11 / Edge	usuario		86		C:\Users\usuario\AppData\Local\Microsoft\Windows\WebCache\WebCacheV01.dat	22
    https://www.binance.com/es/register?ref=KEJHR24A		13/06/2021 13:28:06	1	https://katie.v4.omgtnc.com/api/user/01baf62baecc807cff196e6722da82538fd071ada4.r?tk=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJwdWIiOiI1MDVjNmI4MTcxMzIwNDAyNTc1YjFkNmUiLCJ0cyI6IjA2MTMxMTI4IiwiZCI6Im94aWQuaXQifQ.2RbPmSqsPeb-yTry9wUvoC3YW5zgZ2lJXS6ej3E6zj8	Link	Firefox	usuario	13487k28.default-release	48		C:\Users\usuario\AppData\Roaming\Mozilla\Firefox\Profiles\13487k28.default-release\places.sqlite	8
    https://www.fullaprendizaje.com/2016/08/Las-10-mejores-herramientas-para-descifrar-contrasenas-en-Windows-Linux-y-aplicaciones-Web..html	Las 10 mejores herramientas para descifrar contraseñas en Windows, Linux y aplicaciones Web ~ Full aprendizaje	13/06/2021 13:27:53	1	https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=&cad=rja&uact=8&ved=2ahUKEwjL77O6wJTxAhVN4YUKHTGSCegQFjABegQICBAD&url=https%3A%2F%2Fwww.fullaprendizaje.com%2F2016%2F08%2FLas-10-mejores-herramientas-para-descifrar-contrasenas-en-Windows-Linux-y-aplicaciones-Web..html&usg=AOvVaw3oMTL4lgyV15SKI2xNspcF	Link	Firefox	usuario	13487k28.default-release	136		C:\Users\usuario\AppData\Roaming\Mozilla\Firefox\Profiles\13487k28.default-release\places.sqlite	5
    https://www.google.com/search?client=firefox-b-d&q=hydra	hydra - Buscar con Google	13/06/2021 13:28:32	1		Typed URL	Firefox	usuario	13487k28.default-release	56		C:\Users\usuario\AppData\Roaming\Mozilla\Firefox\Profiles\13487k28.default-release\places.sqlite	11
    https://www.google.com/search?client=firefox-b-d&q=instagram	instagram - Buscar con Google	13/06/2021 13:31:29	1		Typed URL	Firefox	usuario	13487k28.default-release	60		C:\Users\usuario\AppData\Roaming\Mozilla\Firefox\Profiles\13487k28.default-release\places.sqlite	29
    https://www.google.com/search?client=firefox-b-d&q=ophcrack	ophcrack - Buscar con Google	13/06/2021 13:29:32	1		Typed URL	Firefox	usuario	13487k28.default-release	59		C:\Users\usuario\AppData\Roaming\Mozilla\Firefox\Profiles\13487k28.default-release\places.sqlite	18
    https://www.google.com/search?client=firefox-b-d&q=programas+de+craking+de+contrase%C3%B1as	programas de cracking de contraseñas - Buscar con Google	13/06/2021 13:27:39	1		Typed URL	Firefox	usuario	13487k28.default-release	91		C:\Users\usuario\AppData\Roaming\Mozilla\Firefox\Profiles\13487k28.default-release\places.sqlite	3
    https://www.google.com/search?q=hydra+descargar&client=firefox-b-d&ei=4OvFYLH7JIGUlwSe4r3IBQ&oq=hydra+descargar&gs_lcp=Cgdnd3Mtd2l6EAMyAggAMgYIABAWEB4yBggAEBYQHjIGCAAQFhAeMgYIABAWEB4yBggAEBYQHjIGCAAQFhAeMgYIABAWEB4yBggAEBYQHjIGCAAQFhAeOgcIABBHELADOgcIABCwAxBDOgoILhCwAxDIAxBDOgUIABCxAzoICC4QsQMQgwE6CAguEMcBEK8BOgIILjoHCAAQsQMQQzoECC4QCjoECAAQCjoKCC4QxwEQrwEQCkoFCDgSATFQkBpYridg_ChoAXACeACAAZUCiAHwC5IBBTEuOS4xmAEAoAEBqgEHZ3dzLXdpesgBDMABAQ&sclient=gws-wiz&ved=0ahUKEwix68_TwJTxAhUByoUKHR5xD1kQ4dUDCA0&uact=5	hydra descargar - Buscar con Google	13/06/2021 13:28:38	1	https://www.google.com/search?client=firefox-b-d&q=hydra	Link	Firefox	usuario	13487k28.default-release	509		C:\Users\usuario\AppData\Roaming\Mozilla\Firefox\Profiles\13487k28.default-release\places.sqlite	12
    https://www.google.com/search?q=twitter&oq=twitter&aqs=chrome.0.0i131i433l2j0i433j0i3j0i131i433j46i433j0i131i433j0i433j0.1657j0j7&sourceid=chrome&ie=UTF-8	twitter - Buscar con Google	13/06/2021 13:32:12	2		Generated	Chrome	usuario	Default	154	0	C:\Users\usuario\AppData\Local\Google\Chrome\User Data\Default\History	1
    https://www.google.com/search?q=twitter&oq=twitter&aqs=chrome.0.0i131i433l2j0i433j0i3j0i131i433j46i433j0i131i433j0i433j0.1657j0j7&sourceid=chrome&ie=UTF-8	twitter - Buscar con Google	13/06/2021 13:32:16	2		Link	Chrome	usuario	Default	154	0	C:\Users\usuario\AppData\Local\Google\Chrome\User Data\Default\History	2
    https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=&cad=rja&uact=8&ved=2ahUKEwjL77O6wJTxAhVN4YUKHTGSCegQFjABegQICBAD&url=https%3A%2F%2Fwww.fullaprendizaje.com%2F2016%2F08%2FLas-10-mejores-herramientas-para-descifrar-contrasenas-en-Windows-Linux-y-aplicaciones-Web..html&usg=AOvVaw3oMTL4lgyV15SKI2xNspcF		13/06/2021 13:27:53	1	https://www.google.com/search?client=firefox-b-d&q=programas+de+craking+de+contrase%C3%B1as	Link	Firefox	usuario	13487k28.default-release	313		C:\Users\usuario\AppData\Roaming\Mozilla\Firefox\Profiles\13487k28.default-release\places.sqlite	4
    https://www.redeszone.net/2019/06/08/hydra-9-0-herramienta-romper-contrasenas/	Hydra 9.0: herramienta para romper contraseñas con fuerza bruta	13/06/2021 13:28:45	1		Link	Firefox	usuario	13487k28.default-release	78		C:\Users\usuario\AppData\Roaming\Mozilla\Firefox\Profiles\13487k28.default-release\places.sqlite	13
    https://www.thc.org/thc-hydra/	Page not found · GitHub Pages	13/06/2021 13:28:22	1		Link	Firefox	usuario	13487k28.default-release	30		C:\Users\usuario\AppData\Roaming\Mozilla\Firefox\Profiles\13487k28.default-release\places.sqlite	10
    https://www.youtube.com/watch?v=HGFhJado4c8		13/06/2021 13:33:50	1			Internet Explorer 10/11 / Edge	usuario		43		C:\Users\usuario\AppData\Local\Microsoft\Windows\WebCache\WebCacheV01.dat	21
    https://www.youtube.com/watch?v=HGFhJado4c8		13/06/2021 13:33:39	2			Internet Explorer 10/11 / Edge	usuario		43		C:\Users\usuario\AppData\Local\Microsoft\Windows\WebCache\WebCacheV01.dat	38

    [...]
    ```

    **Para ver analizar cada navegador por separado:**

    ```batch
    .\Ferramentas\NirSoft\IECacheView /stab "IECache.log"
    .\Ferramentas\NirSoft\Chromecacheview /stab "ChromeCacheView.log"
    .\Ferramentas\NirSoft\MZCacheView /stab "FirefoxCacheView.log"

    [...]
    ```

- ¿Ha descargado algo sospechoso?¿De qué se trata?
    Trátase de `ophcrack` e `thc-hydra`, ambolos dous son software destinado a descubir contrasinais de contas de usuario de windows.

    ```batch
    .\Ferramentas\NirSoft\BrowsingHistoryView.exe /VisitTimeFilterType 1 /HistorySource 2 /LoadIE 1 /LoadFirefox 1/LoadChrome 1 /LoadSafari 1 /stab Historial.log

    [...]
    https://codeload.github.com/vanhauser-thc/thc-hydra/zip/refs/heads/master	thc-hydra-master.zip	13/06/2021 13:29:25	0		Download	Firefox	usuario	13487k28.default-release	73

    [...]

    https://downloads.sourceforge.net/project/ophcrack/ophcrack/3.8.0/ophcrack-3.8.0-bin.zip?ts=gAAAAABgxewkOiZGCoJkF_vz3bc25ZJI9SYprgbXm0lcS7lVckSrsP7zyogOlbrSWtZFrmvrjL7KvCEzuToGHV9gMNCmpNqlCQ%3D%3D&use_mirror=jztkft&r=https%3A%2F%2Fophcrack.sourceforge.io%2F		13/06/2021 13:29:50	1	https://sourceforge.net/projects/ophcrack/files/ophcrack/3.8.0/ophcrack-3.8.0-bin.zip/download	Link	Firefox	usuario	13487k28.default-release	257
    [...]

    https://github.com/vanhauser-thc/thc-hydra	GitHub - vanhauser-thc/thc-hydra: hydra	13/06/2021 13:28:57	1	https://www.redeszone.net/2019/06/08/hydra-9-0-herramienta-romper-contrasenas/	Link	Firefox	usuario	13487k28.default-release	42		C:\Users\usuario\AppData\Roaming\Mozilla\Firefox\Profiles\13487k28.default-release\places.sqlite	14
    https://github.com/vanhauser-thc/thc-hydra/archive/refs/heads/master.zip		13/06/2021 13:29:20	1		Link	Firefox	usuario	13487k28.default-release	72		C:\Users\usuario\AppData\Roaming\Mozilla\Firefox\Profiles\13487k28.default-release\places.sqlite	16
    https://github.com/vanhauser-thc/thc-hydra/releases/tag/v9.2	Release hydra v9.2 · vanhauser-thc/thc-hydra · GitHub	13/06/2021 13:29:03	1	https://github.com/vanhauser-thc/thc-hydra	Link	Firefox	usuario	13487k28.default-release	60		C:\Users\usuario\AppData\Roaming\Mozilla\Firefox\Profiles\13487k28.default-release\places.sqlite	15
    [...]

    https://jztkft.dl.sourceforge.net/project/ophcrack/ophcrack/3.8.0/ophcrack-3.8.0-bin.zip	ophcrack-3.8.0-bin.zip	13/06/2021 13:29:53	0		Download	Firefox	usuario	13487k28.default-release	88
    [...]

    https://ophcrack.sourceforge.io/	Ophcrack	13/06/2021 13:29:35	1		Link	Firefox	usuario	13487k28.default-release	32		C:\Users\usuario\AppData\Roaming\Mozilla\Firefox\Profiles\13487k28.default-release\places.sqlite	19    https://ophcrack.sourceforge.io/download.php?type=ophcrack	Ophcrack	13/06/2021 13:29:38	1	https://ophcrack.sourceforge.io/	Link	Firefox	usuario	13487k28.default-release	58
    [...]

    https://sourceforge.net/projects/ophcrack/files/ophcrack/3.8.0/ophcrack-3.8.0-bin.zip/download	Download ophcrack from SourceForge.net	13/06/2021 13:29:42	1	https://ophcrack.sourceforge.io/download.php?type=ophcrack	Link	Firefox	usuario	13487k28.default-release	94		C:\Users\usuario\AppData\Roaming\Mozilla\Firefox\Profiles\13487k28.default-release\places.sqlite	21    https://sourceforge.net/projects/ophcrack/postdownload	Find out more about ophcrack | SourceForge.net	13/06/2021 13:30:30	1		Link	Firefox	usuario	13487k28.default-release	54
    [...]

    https://www.google.com/search?client=firefox-b-d&q=hydra	hydra - Buscar con Google	13/06/2021 13:28:32	1		Typed URL	Firefox	usuario	13487k28.default-release	56
    [...]

    https://www.google.com/search?client=firefox-b-d&q=ophcrack	ophcrack - Buscar con Google	13/06/2021 13:29:32	1		Typed URL	Firefox	usuario	13487k28.default-release	59
    [...]

    https://www.google.com/search?q=hydra+descargar&client=firefox-b-d&ei=4OvFYLH7JIGUlwSe4r3IBQ&oq=hydra+descargar&gs_lcp=Cgdnd3Mtd2l6EAMyAggAMgYIABAWEB4yBggAEBYQHjIGCAAQFhAeMgYIABAWEB4yBggAEBYQHjIGCAAQFhAeMgYIABAWEB4yBggAEBYQHjIGCAAQFhAeOgcIABBHELADOgcIABCwAxBDOgoILhCwAxDIAxBDOgUIABCxAzoICC4QsQMQgwE6CAguEMcBEK8BOgIILjoHCAAQsQMQQzoECC4QCjoECAAQCjoKCC4QxwEQrwEQCkoFCDgSATFQkBpYridg_ChoAXACeACAAZUCiAHwC5IBBTEuOS4xmAEAoAEBqgEHZ3dzLXdpesgBDMABAQ&sclient=gws-wiz&ved=0ahUKEwix68_TwJTxAhUByoUKHR5xD1kQ4dUDCA0&uact=5	hydra descargar - Buscar con Google	13/06/2021 13:28:38	1	https://www.google.com/search?client=firefox-b-d&q=hydra	Link	Firefox	usuario	13487k28.default-release	509		C:\Users\usuario\AppData\Roaming\Mozilla\Firefox\Profiles\13487k28.default-release\places.sqlite	12
    [...]

    https://www.redeszone.net/2019/06/08/hydra-9-0-herramienta-romper-contrasenas/	Hydra 9.0: herramienta para romper contraseñas con fuerza bruta	13/06/2021 13:28:45	1		Link	Firefox	usuario	13487k28.default-release	78
    [...]

    https://www.thc.org/thc-hydra/	Page not found · GitHub Pages	13/06/2021 13:28:22	1		Link	Firefox	usuario	13487k28.default-release	30		C:\Users\usuario\AppData\Roaming\Mozilla\Firefox\Profiles\13487k28.default-release\places.sqlite	10
    [...]
    ```

- ¿Qué carpetas compartidas existen?

    ```batch
    net share > CarpetasCompartidas.log

    Nombre       Recurso                         Descripci�n

    -------------------------------------------------------------------------------
    C$           C:\                             Recurso predeterminado
    IPC$                                         IPC remota
    E$           E:\                             Recurso predeterminado
    ADMIN$       C:\Windows                      Admin remota
    compartidaparaMaldades
                C:\compartidaparaMaldades
    Se ha completado el comando correctamente.
    ```

- ¿Existe alguna unidad de red mapeada?
    Existe unha saída mapeada pero é a que lle puxen eu para gardar as evidencias.

    ```batch
    net use > path_saidasMapeadas.log

    Se registrar�n las nuevas conexiones.

    Estado       Local     Remoto                    Red

    -------------------------------------------------------------------------------
                Z:        \\VBoxSvr\Descargas       VirtualBox Shared Folders
    Se ha completado el comando correctamente.
    ```

- ¿Has conseguido alguna contraseña guardada en los navegadores?
    **Os contrasinais sacados son:**

    ```batch
    User Name         : hacker__desesperado@hotmail.com
    Password          : contrasinalPot4nt3

    User Name         : hacker__desesperado@hotmail.com
    Password          : C0ntras345&$nha

    User Name         : hacker__desesperado@hotmail.com
    Password          : ve4s$·Ç"+234078afq23345
    ```

    **Comando execudato:**

    ```batch
    .\Ferramentas\NirSoft\WebBrowserPassView /stab

    ==================================================
    URL               : https://twitter.com/
    Web Browser       : Internet Explorer 10.0
    User Name         : hacker__desesperado@hotmail.com
    Password          : contrasinalPot4nt3
    Password Strength : Very Strong
    User Name Field   :
    Password Field    :
    Created Time      :
    Modified Time     : 13/06/2021 13:33:09
    Filename          :
    ==================================================

    ==================================================
    URL               : https://www.dropbox.com
    Web Browser       : Firefox 32+
    User Name         : hacker__desesperado@hotmail.com
    Password          : C0ntras345&$nha
    Password Strength : Very Strong
    User Name Field   : login_email
    Password Field    : login_password
    Created Time      : 13/06/2021 13:31:20
    Modified Time     : 13/06/2021 13:31:20
    Filename          : C:\Users\usuario\AppData\Roaming\Mozilla\Firefox\Profiles\13487k28.default-release\logins.json
    ==================================================

    ==================================================
    URL               : https://www.instagram.com
    Web Browser       : Firefox 32+
    User Name         : hacker__desesperado@hotmail.com
    Password          : ve4s$·Ç"+234078afq23345
    Password Strength : Very Strong
    User Name Field   : username
    Password Field    : password
    Created Time      : 13/06/2021 13:31:49
    Modified Time     : 13/06/2021 13:31:49
    Filename          : C:\Users\usuario\AppData\Roaming\Mozilla\Firefox\Profiles\13487k28.default-release\logins.json
    ==================================================
    ```

- ¿Ha accedido o modificado algún fichero sospechoso dentro de sus carpetas de usuario?
    **Ficheiros creados:**

    ```batch
    dir /t:c /a /s /o:d c:\ > ListadoFicherosPorFechaDeCreacion.log

    El volumen de la unidad C no tiene etiqueta.
    El n�mero de serie del volumen es: DE59-55D4

    Directorio de c:\

    13/06/2021  12:24    <DIR>          compartidaparaMaldades

    [...]

    :: Aquí están os ficheiros máis recentes
    Directorio de c:\Users\usuario\AppData\Roaming\Microsoft\Windows\Recent

    [...]

    13/06/2021  12:25               572 compartidaparaMaldades.lnk
    13/06/2021  12:29               631 thc-hydra-master.lnk
    13/06/2021  12:29               641 ophcrack-3.8.0-bin.lnk
    13/06/2021  12:35               705 ComoEjecutarExploits.lnk
    13/06/2021  12:36               745 ComoHacerDoSaServidorEmpresa.lnk

    [...]

    Directorio de c:\Users\usuario\Documents

    13/06/2021  12:35               226 ComoEjecutarExploits.rtf
    13/06/2021  12:37                50 ComoHacerDoSaServidorEmpresa.txt

    Directorio de c:\Users\usuario\Downloads

    13/06/2021  12:29           905.813 thc-hydra-master.zip
    13/06/2021  12:29        15.839.258 ophcrack-3.8.0-bin.zip

    [...]
    ```

    **Ficheiros accedidos:**

    ```batch
    dir /t:a /a /s /o:d c:\ > ListadoFicherosPorUltimoAcceso.log

    El volumen de la unidad C no tiene etiqueta.
    El n�mero de serie del volumen es: DE59-55D4

    Directorio de c:\

    02/12/2021  21:17    <DIR>          compartidaparaMaldades

    [...]

    Directorio de c:\Users\usuario\AppData\Roaming\Microsoft\Windows\Recent

    13/06/2021  12:25               572 compartidaparaMaldades.lnk
    13/06/2021  12:29               631 thc-hydra-master.lnk
    13/06/2021  12:29               641 ophcrack-3.8.0-bin.lnk
    13/06/2021  12:35               705 ComoEjecutarExploits.lnk
    13/06/2021  12:36               745 ComoHacerDoSaServidorEmpresa.lnk

    [...]

    Directorio de c:\Users\usuario\Downloads

    13/06/2021  12:29           905.813 thc-hydra-master.zip
    13/06/2021  12:29        15.839.258 ophcrack-3.8.0-bin.zip

    [...]
    ```

    **Ficheiros modificados:**

    ```batch
    dir /t:w /a /s /o:d c:\ > ListadoFicherosPorFechaDeModificacion.log

    El volumen de la unidad C no tiene etiqueta.
    El n�mero de serie del volumen es: DE59-55D4

    Directorio de c:\

    13/06/2021  12:24    <DIR>          compartidaparaMaldades

    [...]

    Directorio de c:\Users\usuario\AppData\Roaming\Microsoft\Windows\Recent

    13/06/2021  12:25               572 compartidaparaMaldades.lnk
    13/06/2021  12:29               631 thc-hydra-master.lnk
    13/06/2021  12:29               641 ophcrack-3.8.0-bin.lnk

    [...]

    Directorio de c:\Users\usuario\Downloads

    13/06/2021  12:29           905.813 thc-hydra-master.zip
    13/06/2021  12:29        15.839.258 ophcrack-3.8.0-bin.zip

    [...]

    ```

> Acompaña las respuestas capturando el comando utilizado y el resultado del mismo marcando en la captura, si es necesario, donde se encuentra la respuesta (vale tanto por consola como en el fichero de texto generado).

## Redline

Trata de contestar a las mismas preguntas recolectando evidencias mediante Redline. Indica aquellas que no puedes obtener.

> Acompaña las respuestas con la captura correspondiente de la consola de visualización de evidencias de Redline.

- ¿Qué usuarios y grupos existen en el equipo?
    ![usuarios](./img/usuarios.png)

- ¿Ha realizado el usuario alguna navegación por Internet “sospechosa”?¿Con qué navegador?
    Buscouse información sobre como crackear contrasinais de windows e tutoriais de como explotar exploits. Empregou o `Firefox`.

    ![historial1](./img/historial1.png)

    **Páxinas invisibles:**
    ![historial2](./img/historial2.png)

- ¿Ha descargado algo sospechoso?¿De qué se trata?
    Trátase de `ophcrack` e `thc-hydra`, ambolos dous son software destinado a descubir contrasinais de contas de usuario de windows.

    ![descargas](./img/descargas.png)

- ¿Qué carpetas compartidas existen?
    A través do explorador do rexistro na clave: `HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\LanmanServer\Shares`
    ![carpetas_compartidas](./img/carpetas_compartidas.png)

- ¿Existe alguna unidad de red mapeada?
    Non vin ningún sitio de onde poder sacar esa información.

- ¿Has conseguido alguna contraseña guardada en los navegadores?
    Non vin ningún sitio de onde poder sacar esa información.

- ¿Ha accedido o modificado algún fichero sospechoso dentro de sus carpetas de usuario?
    **Ficheiros creados:**
    ![ficheiros_creados1](./img/ficheiros_creados1.png)
    ![ficheiros_creados2](./img/ficheiros_creados2.png)

    **Ficheiros accedidos:**
    ![ficheiros_accedidos1](./img/ficheiros_accedidos1.png)
    ![ficheiros_accedidos2](./img/ficheiros_accedidos2.png)

    **Ficheiros modificados:**
    ![ficheiros_modificados1](./img/ficheiros_modificados1.png)
    ![pficheiros_modificados2](./img/ficheiros_modificados2.png)
