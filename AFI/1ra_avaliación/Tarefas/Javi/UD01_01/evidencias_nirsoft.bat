:: Lista as path_saidas de disco
:: for %a in (%OUTPUT%) do echo %a
:: for /f "tokens=*" %a in ('fsutil fsinfo drives') do SET OUTPUT=%a
:: echo %OUTPUT%

@echo off
set dataIni=%date% %time%

:: Hora INICIO execución
:: set dataIni=powershell -command "'%date%_%time%' -replace '/', '_' -replace ':', '_' -replace ',','_'"
:: @echo %dataIni%
:: echo %dataIni% > %path_saida%00_inicio.log

cls
fsutil fsinfo drives
set /p unidade="Escolle unha path_saida de disco onde gardar as evicendcias: "
echo path_saida seleccionada %unidade%

set /p carpeta="Escolle un nome para a carpeta de evidencias: "
set path_saida=%unidade%:\%carpeta%\

set /p nome_usuario="Nome do usuario actual: "

echo Path completo %path_saida%
mkdir %path_saida%

@echo %dataIni%
echo %dataIni% > %path_saida%00_inicio.log

:: Información de red y conexiones
ipconfig /all > %path_saida%ConfiguracionRede.log
ipconfig /displaydns > %path_saida%DNSCache.log
arp -a > %path_saida%ArpCache.log
netstat -an | findstr /i “estado listening established” > %path_saida%PuertosAbertos.log
netstat -anob > %path_saida%AplicacionsConPortosAbertos.log
netstat -r > %path_saida%TablaEnrutamento.log
nbtstat -S > %path_saida%ConexionsNetbiosEstablecidas.log
.\Ferramentas\NirSoft\urlprotocolview /stext  %path_saida%ProtocolosRede.log

:: Ficheiros y carpetas compartidas
net use > %path_saida%path_saidasMapeadas.log
net share > %path_saida%CarpetasCompartidas.log
net file > %path_saida%FicheirosCopiadosMedianteNetbios.log
.\Ferramentas\SysinternalsSuite\psfile /accepteula > %path_saida%FicheirosAbertosRemotamente.log

:: Histórico de comandos de consola
doskey /history > %path_saida%HistoricoComandos.log

:: Procesos
tasklist > %path_saida%ProcesosEnExecucion.log
.\Ferramentas\SysinternalsSuite\pslist /accepteula /t > %path_saida%ArboreDeProcesosEnExecucion.log
.\Ferramentas\SysinternalsSuite\listdlls /accepteula > %path_saida%ProcesosEDlls.log
.\Ferramentas\SysinternalsSuite\handle /accepteula /a > %path_saida%Handles.log
.\Ferramentas\NirSoft\cprocess /stext  %path_saida%ProcesosUsuario.log
sc query > %path_saida%ServiciosEnExecucion.log

:: Información de usuarios
nbtstat -n > %path_saida%UsuariosEGruposNetbios.log
net localgroup > %path_saida%GruposEquipo.log
net user > %path_saida%UsuariosEquipo.log
net user %nome_usuario% > %path_saida%InformacionUsuario_%nome_usuario%.log
net session > %path_saida%UsuariosRemotos.log
.\Ferramentas\SysinternalsSuite\logonsessions /accepteula > %path_saida%SesionsActivas.log
.\Ferramentas\SysinternalsSuite\psloggedon /accepteula > %path_saida%UsuariosSesionIniciada.log

:: Contraseñas
.\Ferramentas\NirSoft\WebBrowserPassView /stab "%path_saida%ClavesNavegadores.log"
.\Ferramentas\NirSoft\mailpv /stab "%path_saida%MailPassView.log"
.\Ferramentas\NirSoft\IECacheView /stab "%path_saida%IECache.log"

:: Información cacheada en los navegadores (direcciones, historial de descargas)
.\Ferramentas\NirSoft\IECacheView /stab "%path_saida%IECache.log"
.\Ferramentas\NirSoft\Chromecacheview /stab "%path_saida%ChromeCacheView.log"
.\Ferramentas\NirSoft\MZCacheView /stab "%path_saida%FirefoxCacheView.log"

:: Árbol de directorios y ficheros
dir /t:w /a /s /o:d c:\ > "%path_saida%ListadoFicheirosPorDataModificacion.log"
dir /t:a /a /s /o:d c:\ > "%path_saida%ListadoFicheirosPorUltimoAcceso.log"
dir /t:c /a /s /o:d c:\ > "%path_saida%ListadoFicheirosPorDataCreacion.log"

:: Historial de internet
.\Ferramentas\NirSoft\BrowsingHistoryView.exe /VisitTimeFilterType 1 /HistorySource 2 /LoadIE 1 /LoadFirefox 1/LoadChrome 1 /LoadSafari 1 /stab %path_saida%Historial.log

:: Últimas búsquedas
.\Ferramentas\NirSoft\MyLastSearch /stab "%path_saida%MyLastSearch.log"

:: Cookies

:: Volúmenes cifrados
.\Ferramentas\eedh\eedh.exe > "%path_saida%VolumensCifrados.log"

:: Otros
type c:\windows\system32\drivers\etc\hosts > %path_saida%Hosts.log
:: .\Ferramentas\SysinternalsSuite\openedfilesview /stext > %path_saida%FicheirosAbertos.log
net stats srv > %path_saida%ServiciosArranqueSistema.log
net stats workstation > %path_saida%ArranqueSistema.log

.\Ferramentas\NirSoft\insideclipboard /saveclp > %path_saida%Portapapeis.clp
systeminfo > %path_saida%InformacionSistema.log

:: Hora FIN execución
set dataFin=%date% %time%
@echo %dataFin%
echo %dataFin% > %path_saida%00_fin.log
