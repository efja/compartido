# UD01-T01 Recolección de evidencias volátiles Windows 10

## Enunciado

Dada a captura de memoria dunha máquina Windows 7 x64 SP1 en R:\MVirtuais\ciberCE\AnaliseFI\volatiles\Caso1Volatility.zip, (pódela descargar na casa desde a seguinte [ligazón](https://mega.nz/file/1cEHWSDT#sKmxwy_42-tBXzO_STCwSoP4KRwO95R3bp4v8oBvuh0)) utiliza volatility para contestar ás seguintes preguntas:

1. Cales serían os pasos previos a realizar antes de empezar a analizar o ficheiro descargado? **1 pto**
2. Cal é o nome do equipo? **2 pts**
3. O usuario tiña establecida unha conexión ftp cun organismo público. Cal é? **1 pto**
4. Hai polo menos un proceso que contén malware. Cal é o seu nome e PID? Como podemos comprobar que é un proceso maligno? _(Pista: investigar como facer uso do sitio [VIRUSTOTAL](https://www.virustotal.com/gui/home/upload))_ **2 pts**
5. Hai un proceso infectado que ten establecida unha conexión HTTPS. Cal é a dirección IP á que está conectado?. Deberás de xustificar que é un proceso infectado. **2 pts**
6. Hai un contrasinal dun ficheiro comprimido escrita dentro do Bloc de Notas. Cal é? **1 pto**
7. Existe un ficheiro zip accesible na memoria RAM. Que animal se atopa dentro? **1 pto**

## Solución

### 1. Cales serían os pasos previos a realizar antes de empezar a analizar o ficheiro descargado? **1 pto**

Hai que verificar que o `hash` coincide co que se obtivo de xeito seguro para verificar a cadea de custodia.

#### 1.1. Procendemento

```bash
# lectura do hash do ficheiro txt e cálculo do ficheiro da imaxe
cat caso1Volatility.dmp.MD5.txt && md5sum caso1Volatility.dmp

# saída
8922965d14b8a0ef240b62d460e6146e
8922965d14b8a0ef240b62d460e6146e  caso1Volatility.dmp
```

<div style="page-break-after: always;"></div>

Análise previa da imaxe acotandoo os perfiles a un `Windows 7 XP1 x64`

```bash
./volatility -f caso1Volatility.dmp --profile=Win7SP1x64 kdbgscan

# saída
Volatility Foundation Volatility Framework 2.6
[...]
**************************************************
Instantiating KDBG using: Kernel AS Win7SP1x64 (6.1.7601 64bit)
Offset (V)                    : 0xfa8003321080
Offset (P)                    : 0x67f69080
KDBG owner tag check          : True
Profile suggestion (KDBGHeader): Win7SP1x64_23418
Service Pack (CmNtCSDVersion) : 1
Build string (NtBuildLab)     : 7601.17514.amd64fre.win7sp1_rtm.
PsActiveProcessHead           : 0xfffff80002840b90 (52 processes)
PsLoadedModuleList            : 0xfffff8000285ee90 (139 modules)
KernelBase                    : 0xfffff80002619000 (Matches MZ: True)
Major (OptionalHeader)        : 6
Minor (OptionalHeader)        : 1
KPCR                          : 0xfffff8000280bd00 (CPU 0)
**************************************************
[...]
```

<div style="page-break-after: always;"></div>

### 2. Cal é o nome do equipo? **2 pts**

O nome é `W7BASE`.

#### 2.1. Procendemento

Determinando as direccións de memoria onde están cargados os ficheiros do rexistro (o que nos interesa é a clave `\REGISTRY\MACHINE\SYSTEM`)

```bash
./volatility -f caso1Volatility.dmp --profile=Win7SP1x64_23418 hivelist

# saída
Volatility Foundation Volatility Framework 2.6
Virtual            Physical           Name
------------------ ------------------ ----
0xfffff8a0086e6010 0x0000000020ab0010 \??\C:\Users\wadmin\AppData\Local\Microsoft\Windows\UsrClass.dat
0xfffff8a00000d010 0x000000002d4c0010 [no name]
0xfffff8a000024010 0x000000002d525010 \REGISTRY\MACHINE\SYSTEM
0xfffff8a00005e010 0x000000002d4e1010 \REGISTRY\MACHINE\HARDWARE
0xfffff8a0001b7010 0x000000002cf63010 \Device\HarddiskVolume1\Boot\BCD
0xfffff8a0009f0160 0x0000000054384160 \SystemRoot\System32\Config\DEFAULT
0xfffff8a0009fb010 0x000000002d0db010 \SystemRoot\System32\Config\SOFTWARE
0xfffff8a000fb8010 0x0000000024de0010 \??\C:\Windows\ServiceProfiles\NetworkService\NTUSER.DAT
0xfffff8a001076010 0x0000000024b9c010 \??\C:\Windows\ServiceProfiles\LocalService\NTUSER.DAT
0xfffff8a001260410 0x00000000214f6410 \??\C:\Windows\System32\config\COMPONENTS
0xfffff8a0037bd010 0x0000000029f52010 \SystemRoot\System32\Config\SAM
0xfffff8a0037c0010 0x0000000029f97010 \SystemRoot\System32\Config\SECURITY
0xfffff8a005fe2010 0x000000001a736010 \??\C:\System Volume Information\Syscache.hve
0xfffff8a00864f010 0x00000000210c2010 \??\C:\Users\wadmin\ntuser.dat
```

<div style="page-break-after: always;"></div>

Coa información da anterior lemos a clave do rexistro onde se garda o nome do equipo

```bash
./volatility -f caso1Volatility.dmp --profile=Win7SP1x64_23418 printkey -o 0xfffff8a000024010 -K "ControlSet001\Control\ComputerName\ComputerName"

# saída
Volatility Foundation Volatility Framework 2.6
Legend: (S) = Stable   (V) = Volatile

----------------------------
Registry: \REGISTRY\MACHINE\SYSTEM
Key name: ComputerName (S)
Last updated: 2016-02-16 11:57:10 UTC+0000

Subkeys:

Values:
REG_SZ                        : (S) mnmsrvc
REG_SZ        ComputerName    : (S) W7BASE
```

**Outra alternativa é acceder á variable de entorno `COMPUTERNAME`**
Primeiro habería que executar o `pslist` ou similar para sacar un `PID` e despois buscar a variable en cuestión, se non se fixese sobre un proceso concreto tardaría moito porque listaríanse as variables de entorno de tódolos procesos e sen agrupar as que son comúns polo que se listarían moitas repetidas.

```bash
./volatility -f caso1Volatility.dmp --profile=Win7SP1x64_23418 envars -p 2100 | grep COMPUTERNAME

# saída
Volatility Foundation Volatility Framework 2.6
    2100 WmiPrvSE.exe         0x0000000000061320 COMPUTERNAME                   W7BASE
```

<div style="page-break-after: always;"></div>

### 3. O usuario tiña establecida unha conexión ftp cun organismo público. Cal é? **1 pto**

O organismo é `rediris.es`

#### 3.1. Procendemento

Identificando as conexións `ftp` (porto 21) abertas:

```bash
./volatility -f caso1Volatility.dmp --profile=Win7SP1x64_23418 netscan | grep :21

# saída
Volatility Foundation Volatility Framework 2.6
Offset(P)          Proto    Local Address                  Foreign Address      State            Pid      Owner          Created
0x8616250          UDPv4    0.0.0.0:0                      *:*                                   1072     svchost.exe    2020-12-07 18:19:21 UTC+0000
0x8616250          UDPv6    :::0                           *:*                                   1072     svchost.exe    2020-12-07 18:19:21 UTC+0000
0x33e94600         UDPv6    ::1:1900                       *:*                                   1408     svchost.exe    2020-12-07 18:21:20 UTC+0000
0x33e94ec0         UDPv6    fe80::94c2:d446:5bf2:fe9a:1900 *:*                                   1408     svchost.exe    2020-12-07 18:21:20 UTC+0000
0x45abcc10         UDPv4    10.0.2.15:138                  *:*                                   4        System         2020-12-07 18:19:21 UTC+0000
0x5b034ae0         TCPv4    10.0.2.15:49171                130.206.13.2:21      ESTABLISHED      2424     ftp.exe
0x66cd84f0         UDPv4    127.0.0.1:1900                 *:*                                   1408     svchost.exe    2020-12-07 18:21:20 UTC+0000
0x66cd8bb0         UDPv4    10.0.2.15:1900                 *:*                                   1408     svchost.exe    2020-12-07 18:21:20 UTC+0000
0x6ab41d70         UDPv4    10.0.2.15:137                  *:*                                   4        System         2020-12-07 18:19:21 UTC+0000
0x72693800         UDPv4    127.0.0.1:63920                *:*                                   1408     svchost.exe    2020-12-07 18:21:20 UTC+0000
0x72693ec0         UDPv6    ::1:63919                      *:*                                   1408     svchost.exe    2020-12-07 18:21:20 UTC+0000
0x7e9ddae0         TCPv4    10.0.2.15:49171                130.206.13.2:21      ESTABLISHED      2424     ftp.exe
```

Vendo a información das conexións vese que a conexión foi establecida co programa de consola `ftp.exe`.

<div style="page-break-after: always;"></div>

Analizando os comandos executados en consola para busar a cadea de conexión de `ftp.exe`

```bash
./volatility -f caso1Volatility.dmp --profile=Win7SP1x64_23418 cmdscan

# saída
Volatility Foundation Volatility Framework 2.6
**************************************************
CommandProcess: conhost.exe Pid: 2396
CommandHistory: 0x1d44c0 Application: cmd.exe Flags: Allocated, Reset
CommandCount: 1 LastAdded: 0 LastDisplayed: 0
FirstCommand: 0 CommandCountMax: 50
ProcessHandle: 0x5c
Cmd #0 @ 0x1d0490: ftp ftp.rediris.es
Cmd #15 @ 0x190158:
Cmd #16 @ 0x1d2df0:
**************************************************
CommandProcess: conhost.exe Pid: 2396
CommandHistory: 0x1d8ba0 Application: ftp.exe Flags: Allocated, Reset
CommandCount: 1 LastAdded: 0 LastDisplayed: 0
FirstCommand: 0 CommandCountMax: 50
ProcessHandle: 0xdc
Cmd #0 @ 0x1d04c0: pruebaprueba
**************************************************
[...]
```

### 4. Hai polo menos un proceso que contén malware. Cal é o seu nome e PID? Como podemos comprobar que é un proceso maligno? _(Pista: investigar como facer uso do sitio [VIRUSTOTAL](https://www.virustotal.com/gui/home/upload))_ **2 pts**

Name                PID   PPID
---------------- ------ ------
cmd.exe            3428   3296
ftp.exe            2424   2388
AsustoMucho.ex     1004    896
ftp.exe            2424   2388
pytcw.exe          3996   1640
AsustoMucho.ex     1004    896
cmd.exe            3428   3296
7zFM.exe           1484    896

#### 4.1. Procendemento

Identificación dos procesos activos

```bash
# Captura dos procesos a un directorio seguro
./volatility -f caso1Volatility.dmp --profile=Win7SP1x64_23418 procdump -D dump/
```

Despois de pasar un pequeno script en `Python` que adxunto coa tarefa para sacar os hashes de tódolos procesos extraídos e pasar esa lista por [VIRUSTOTAL](https://www.virustotal.com/gui/home/upload) identificou estos procesos como perigosos

```cmd
059810aa0cf5d96af398a6ee8752bf6ef025b4a900718ee45ba3b87c6baa8711  executable.3996.exe
25b31a415bb36fa5edf30f0647ce5fc4f2a028bead3586b02f11e2313020a662  executable.996.exe
2d9722d7bb31b9810deb3490a35f2aeb31560e7f6d718f586678449bf547278b  executable.3428.exe
4441f5c3b4b935f9559c4dbde955d73a844f3c41c4d25627c0b29d4143fbf1b5  executable.2424.exe
60f544c8ea2a0102fc9dfb5e2591385ef3759a623f2ab889285c9760ac61a2d5  executable.1484.exe
637c9f119bc4604db19018e832e8f31e9307f5e6edcbd0eb2e9aa7089dc1e116  executable.1004.exe
```

Fixen outro escaneo de procesos para avaliar tamén os procesos ocultos.

```bash
./volatility -f caso1Volatility.dmp --profile=Win7SP1x64_23418 psscan

# saída
Volatility Foundation Volatility Framework 2.6
Offset(P)          Name                PID   PPID PDB                Time created                   Time exited
------------------ ---------------- ------ ------ ------------------ ------------------------------ ------------------------------
[...]
0x0000000043126b30 cmd.exe            3428   3296 0x000000002fd25000 2020-12-07 18:29:33 UTC+0000
0x000000006ca80890 ftp.exe            2424   2388 0x000000000fe34000 2020-12-07 18:19:50 UTC+0000
0x000000007b583b30 AsustoMucho.ex     1004    896 0x0000000016366000 2020-12-07 18:19:31 UTC+0000
0x000000007e668890 ftp.exe            2424   2388 0x000000000fe34000 2020-12-07 18:19:50 UTC+0000
0x000000007e851b30 pytcw.exe          3996   1640 0x0000000067419000 2020-12-07 18:58:36 UTC+0000
0x000000007e8e8b30 AsustoMucho.ex     1004    896 0x0000000016366000 2020-12-07 18:19:31 UTC+0000
0x000000007ee7db30 cmd.exe            3428   3296 0x000000002fd25000 2020-12-07 18:29:33 UTC+0000
0x000000007ef1b060 7zFM.exe           1484    896 0x0000000033035000 2020-12-07 18:53:53 UTC+0000
[...]
```

<div style="page-break-after: always;"></div>

### 5. Hai un proceso infectado que ten establecida unha conexión HTTPS. Cal é a dirección IP á que está conectado?. Deberás de xustificar que é un proceso infectado. **2 pts**

O proceso é `pytcw.exe`.

#### 5.1. Procendemento

Identificando as conexións `https` (porto 443) abertas:

```bash
./volatility -f caso1Volatility.dmp --profile=Win7SP1x64_23418 netscan | grep :443

# saída
Volatility Foundation Volatility Framework 2.6
0x36281010         TCPv4    10.0.2.15:49262                216.58.213.3:443     ESTABLISHED      1608     firefox.exe
0x3aab7010         TCPv4    -:49284                        33.161.122.39:443    ESTABLISHED      1608     firefox.exe
0x3abc2010         TCPv4    -:49263                        188.165.205.194:443  CLOSED           1608     firefox.exe
0x464ebcf0         TCPv4    -:49286                        118.45.153.189:443   ESTABLISHED      1608     firefox.exe
0x62341010         TCPv4    -:49285                        -:443                ESTABLISHED      1608     firefox.exe
0x7fa7ecf0         TCPv4    10.0.2.15:49325                2.19.61.200:443      ESTABLISHED      2464     iexplore.exe
0x7fdf0580         TCPv4    10.0.2.15:49326                2.19.61.200:443      ESTABLISHED      2464     iexplore.exe
0x7fe0f450         TCPv4    10.0.2.15:49525                160.153.75.34:443    ESTABLISHED      3996     pytcw.exe
```

```bash
# Captura dos procesos a un directorio seguro
./volatility -f caso1Volatility.dmp --profile=Win7SP1x64_23418 procdump -p 3996 -D dump/

# saída
0xfffffa8002eb1b30 0x0000000000400000 pytcw.exe            OK: executable.3996.exe

# Sacando hash
sha256sum dump/executable.3996.exe
# saída
059810aa0cf5d96af398a6ee8752bf6ef025b4a900718ee45ba3b87c6baa8711  dump/executable.3996.exe
```

Comprobada información en VIRUSTOTAL:
https://www.virustotal.com/gui/file/059810aa0cf5d96af398a6ee8752bf6ef025b4a900718ee45ba3b87c6baa8711

### 6. Hai un contrasinal dun ficheiro comprimido escrita dentro do Bloc de Notas. Cal é? **1 pto**

A clave é `abc123..`

#### 6.1. Procendemento

Escaneo da memoria buscando procesos de `notepad` abertos

```bash
./volatility -f caso1Volatility.dmp --profile=Win7SP1x64_23418 pslist | grep notepad

# saída
0xfffffa80022b2b30 notepad.exe            2732    896      4      291      1      0 2020-12-07 18:20:23 UTC+0000
```

Facendo volcado da memoria para o proceso do `notepad`

```bash
./volatility -f caso1Volatility.dmp --profile=Win7SP1x64_23418 memdump -p 2732 --dump-dir=dump/

# saída
************************************************************************
Writing notepad.exe [  2732] to 2732.dmp
```

Sacando a saída a un ficheiro txt.

```bash
strings -e l dump/2732.dmp > notepad.txt
```

Despois busquei coa función de búsqueda do `vscode` os patróns `password`, `passwd`, `pass`, `clave`, `contrasinal` e `contraseña`. Cando estaba escribindo esta última coa sección `contrase` atopei o seguinte texto:

```text
la contrase
a del zip es abc123..
```

<div style="page-break-after: always;"></div>

#### 6.2. Procendemento

Un procedemento máis directo foi probando o plugin experimental `editbox` (saqueino do --help).

```bash
./volatility -f caso1Volatility.dmp --profile=Win7SP1x64_23418 editbox

# saída
Volatility Foundation Volatility Framework 2.6
[...]
******************************
Wnd Context       : 1\WinSta0\Default
Process ID        : 2732
ImageFileName     : notepad.exe
IsWow64           : No
atom_class        : 6.0.7601.17514!Edit
value-of WndExtra : 0x260700
nChars            : 33
selStart          : 33
selEnd            : 33
isPwdControl      : False
undoPos           : 18
undoLen           : 1
address-of undoBuf: 0x266ba0
undoBuf           : x
-------------------------
la contraseña del zip es abc123..
```

<div style="page-break-after: always;"></div>

### 7. Existe un ficheiro zip accesible na memoria RAM. Que animal se atopa dentro? **1 pto**

Un `koala`

#### 7.1. Procendemento

Buscando ventás abertas con algún ficheiro `zip` aberto.

```bash
./volatility -f caso1Volatility.dmp --profile=Win7SP1x64 windows | grep zip

# saída
Volatility Foundation Volatility Framework 2.6
Window Handle: #140502 at 0xfffff900c064c200, Name: C:\Users\wadmin\Documents\fichero.zip\
```

Buscando o ficheiro no `fichero.zip` no sistema de ficheiros.

```bash
./volatility -f caso1Volatility.dmp --profile=Win2008R2SP1x64_23418 filescan | grep fichero.zip

# saída
Volatility Foundation Volatility Framework 2.6
0x000000007f52e070      2      1 R--r-- \Device\HarddiskVolume2\Users\wadmin\Documents\fichero.zip
```

Facendo o volcado do ficheiro a un lugar seguro.

```bash
./volatility -f caso1Volatility.dmp --profile=Win2008R2SP1x64_23418 dumpfiles -Q 0x000000007f52e070 -D dump

# saída
Volatility Foundation Volatility Framework 2.6
DataSectionObject 0x7f52e070   None   \Device\HarddiskVolume2\Users\wadmin\Documents\fichero.zip
SharedCacheMap 0x7f52e070   None   \Device\HarddiskVolume2\Users\wadmin\Documents\fichero.zip
```

Descomprimindo o ficheiro `.dat` (asumo que é o tipo é un `zip` porque é o ficheiro que volquei).

```bash
 unzip file.None.0xfffffa8001b72f10.dat

# saída
Archive:  file.None.0xfffffa8001b72f10.dat
  inflating: fichero.jpg
```

#### 7.2. Probas

Probando o plugin experimental `editbox` (saqueino do --help) sacanse os programas abertos que teñen algunha caixa de texto e dase a casualidade de que o software no que estaba aberto o ficheiro `zip` ten unha coa `URI` do mesmo.

```bash
./volatility -f caso1Volatility.dmp --profile=Win7SP1x64_23418 editbox

# saída
Volatility Foundation Volatility Framework 2.6
[...]
******************************
Wnd Context       : 1\WinSta0\Default
Process ID        : 1484
ImageFileName     : 7zFM.exe
IsWow64           : No
atom_class        : 6.0.7601.17514!Edit
value-of WndExtra : 0x50e9d0
nChars            : 38
selStart          : 0
selEnd            : 38
isPwdControl      : False
undoPos           : 0
undoLen           : 0
address-of undoBuf: 0x0
undoBuf           :
-------------------------
C:\Users\wadmin\Documents\fichero.zip\
[...]
```
