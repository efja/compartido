#!/usr/bin/python3
# requisito: pip3 install passlib

######################################################################################################
## IMPORTACIÓNS
######################################################################################################
from __future__ import absolute_import
from __future__ import print_function
from __future__ import unicode_literals

import pathlib
from posixpath import dirname
import subprocess
import sys
import os

######################################################################################################
## VARIABLES GLOBAIS
######################################################################################################
output_file         = '00_hashes.log'
output_file_path    = './{0}'.format(output_file)

hashes_collection   = []

######################################################################################################
## FUNCIÓNS FICHEIROS
######################################################################################################
def create_file():
    """Crea o ficheiro para gardar os logs."""
    os.makedirs(os.path.dirname(output_file_path), exist_ok = True)

    with open(output_file_path, 'w+', encoding='utf-8') as f:
        f.write('')

def save_file(data):
    """Garda a información de cada hash no ficheiro.

    :param data     : hash e nome do ficheiro
    """
    open_file_mode = 'a+'

    # Escríbese a información no ficheiro
    with open(output_file_path, open_file_mode, encoding='utf-8') as f:
        f.writelines(data)

######################################################################################################
## FUNCIÓNS HASH
######################################################################################################
def search_files(uri):
    """Recore o directorio onde se gardan os hashes e executaos."""
    test_names = pathlib.Path(uri)

    for item in test_names.iterdir():
        if (item.is_file() and item.name != 'crear_hash.py' and item.name != output_file):
            calculate_hash(item.name)

def calculate_hash(file_name):
    """O hash co nome pasado como parámetro e crea o log.

    :param file_name : nome do ficheiro a procesar
    """
    temp_command = ['sha256sum', file_name]
    proc = subprocess.run(
        temp_command,
        capture_output=True,
        encoding='UTF-8'
    )

    # Imprimese por pantalla o resultado
    print(proc.stdout)

    # Gárdase a información no ficheiro de log
    save_file(proc.stdout)

######################################################################################################
## FUNCIÓN PRINCIPAL
######################################################################################################
def main():
    """Función principal."""
    args = sys.argv[1:]
    current_dir = '.'

    if len(args) == 1:
        current_dir = args[0]

    create_file()
    search_files(current_dir)

######################################################################################################
## ARRANQUE
######################################################################################################
if __name__ == '__main__':
    main()

