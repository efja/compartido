# Repaso comandos linux

1. Contar número liñas do ficheiro `nmap.md`

    ```bash
    wc -l nmap.md
    ```

2. Encontrar de aparcións dunha palabra `nmap.md`

    ```bash
    grep "open" nmap.md
    ```

3. Contar o número de veces que aparece unha palabra `nmap.md`

    ```bash
    grep -c "open" nmap.md

    grep "open" nmap.md | wc -l
    ```

4. Mostrar as liñas asociadas asociadas ós portos abertod no ficheiro `nmap.md`

    ```bash
    grep "open" nmap.md | grep "/tcp" nmap.md
    ```

5. Contar os portos abertos no ficheiro `nmap.md`

    ```bash
    # liñas buscadas
    grep "open" nmap.md | grep "/tcp"

    # contando
    grep "open" nmap.md | grep -c "/tcp"
    grep "open" nmap.md | grep "/tcp" | wc -l
    ```

6. Atopar e contar as aparcións da palabra `nmap` no ficheiro `nmap.md`

    ```bash
    grep -c "nmap" nmap.md
    ```

7. Atopar e contar as aparcións da palabra `Nmap` no ficheiro `nmap.md`

    ```bash
    grep -c "Nmap" nmap.md
    ```

8. Atopar e contar as aparcións da palabra `Nmap` e `nmap` no ficheiro `nmap.md`

    ```bash
    grep -ci "nmap" nmap.md
    ```

9. Quedarse coas liñas dos portos abertos gardándoas nun ficherio txt co nome `portos.txt`

    ```bash
    grep "open" nmap.md | grep "/tcp" > portos.txt
    ```

10. Quedarse coas liñas dos portos abertos gardándoas nun ficherio txt co nome `portos2.txt` pero vendo a saída por pantalla

    ```bash
    grep "open" nmap.md | grep "/tcp" > portos2.txt | cat
    grep "open" nmap.md | grep "/tcp" > portos2.txt | tee
    grep "open" nmap.md | grep "/tcp" | tee portos2.txt
    ```

11. Mostar en Kali os usuario do ficheiro `/etc/passwd` que teñan unha `shell` do tipo `nologin`

    ```bash
    grep "nologin" /etc/passwd
    ```

12. Mostar en Kali os usuario do ficheiro `/etc/passwd` que teñan unha `shell` dun tipo non `nologin`

    ```bash
    grep "nologin" /etc/passwd
    ```

13. Mostar en Kali os usuario do ficheiro `/etc/passwd` que teñan unha `shell` do tipo `nologin` ou `false`

    ```bash
    grep -e 'nologin' -e 'false' /etc/passwd
    grep -E 'nologin|false' /etc/passwd
    egrep 'nologin|false' /etc/passwd
    ```

14. Mostar en Kali os usuario do ficheiro `/etc/passwd` que non teñan unha `shell` do tipo `nologin` nin `false`

    ```bash
    grep -v -e 'nologin' -e 'false' /etc/passwd
    grep -v -E 'nologin|false' /etc/passwd
    egrep -v 'nologin|false' /etc/passwd
    ```

15. Mostrar por pantalla os nomes dos usuarios do ficheiro `/etc/passwd`

    ```bash
    cut -d ':' -f 1 /etc/passwd
    ```

16. Mostrar por pantalla os nomes dos usuarios, o seu `home` e a `shell` do ficheiro `/etc/passwd`

    ```bash
    cut -d ':' -f 1,6,7 /etc/passwd
    cut -d ':' -f 1,6- /etc/passwd
    ```

17. Mostrar por pantalla os nomes dos usuarios, o seu `home` e a `shell` do ficheiro `/etc/passwd`, sempre que teñan unha `shell`que non sexa do tipo `nologin` nin `false`

    ```bash
    grep -v -e 'nologin' -e 'false' /etc/passwd | cut -d ':' -f 1,6,7
    grep -v -e 'nologin' -e 'false' /etc/passwd | cut -d ':' -f 1,6-
    ```

18. Mostrar por pantalla a `shell` do ficheiro `/etc/passwd`

    ```bash
    cut -d ':' -f 7 /etc/passwd
    ```

19. Mostrar por pantalla a `shell` do ficheiro `/etc/passwd` ordenado

    ```bash
    cut -d ':' -f 7 /etc/passwd | sort
    cut -d ':' -f 7 /etc/passwd | sort -r # reverse
    ```

20. Mostrar por pantalla a `shell` do ficheiro `/etc/passwd` ordenado sen repeticións

    ```bash
    cut -d ':' -f 7 /etc/passwd | sort -u
    cut -d ':' -f 7 /etc/passwd | sort -ur # reverse
    ```

21. Di cantos usuarios teñan cada unha dos tipos `shell`

    ```bash
    cut -d ':' -f 7 /etc/passwd | sort | uniq -c
    ```

22. Di cantos usuarios teñan cada unha dos tipos `shell` ordenados de maior a menor

    ```bash
    cut -d ':' -f 7 /etc/passwd | sort | uniq -c | sort -nr
    ```

23. Mostrar os usuarios empregando `awk`

    ```bash
    awk -F ":" '{ print $1 }' /etc/passwd
    ```

24. Mostrar por pantalla os usuarios do ficheiro `/etc/passwd` xunto co seu directorio `home` e o seu `shell` empregando `awk`

    ```bash
    awk -F ":" '{ print $1, $6, $7 }' /etc/passwd
    ```

25. Idem pero separando os campos con `:`

    ```bash
    awk -F ":" '{ print $1 ":" $6 ":" $7 }' /etc/passwd
    ```

26. Quedarse co campo 1 do ficheiro `portos.txt` usando  `cut`

    ```bash
    cut -d " " -f 1 portos.txt
    ```

27. Quedarse co campo 3 (servicio) do ficheiro `portos.txt` usando `cut`

    ```bash
    cut -d " " -f 3 portos.txt # falla
    ```

28. Repetir anterior usando `awk`

    ```bash
    awk -F " " '{ print $3}' portos.txt
    awk '{ print $3}' portos.txt # se o doimitador é un espazo en branco non fai falta o -F
    ```

## Ficheiro `nmap_A_scan_tcp.txt`

1. Partindo do ficheiro `nmap_A_scan_tcp.txt` facer un listado dos portos abertos e en cantos equipos en total están abertos.

    ```bash
    grep "open" ficheiros/nmap_A_scan_tcp.txt | grep -E "tcp|udp" | awk -F "/" '{ print $1 }' | sort | uniq -c

    grep -w "tcp" ficheiros/nmap_A_scan_tcp.txt | grep -v "|" | awk -F "/" '{ print $1 }' | sort | uniq -c
    grep -w "tcp" ficheiros/nmap_A_scan_tcp.txt | grep "open" | awk -F "/" '{ print $1 }' | sort | uniq -c
    ```

2. Ordena o resultado de maior a menor.

    ```bash
    grep "open" ficheiros/nmap_A_scan_tcp.txt | grep -E "tcp|udp" | awk -F "/" '{ print $1 }' | sort | uniq -c | sort -nr

    grep -w "tcp" ficheiros/nmap_A_scan_tcp.txt | grep -v "|" | awk -F "/" '{ print $1 }' | sort | uniq -c | sort -nr
    grep -w "tcp" ficheiros/nmap_A_scan_tcp.txt | grep "open" | awk -F "/" '{ print $1 }' | sort | uniq -c | sort -nr
    ```

## Ficheiro `nmap_smb_users.txt`

1. Para facer un ataque de contrasinais, extrae unha lista de usuarios a partir do ficheiro n`map_smb_users.txt` e guardala en un ficheiro llamado `usuarios.txt`

    ```bash
    grep -w "METASPLOITABLE" ficheiros/nmap_smb_users.txt | awk -F '\' '{ print $2 }' | awk '{ print $1 }'
    ```

## Log `Apache` --> ficheiro `access_log.txt`

1. Nº de líneas

    ```bash
    wc -l ficheiros/access_log.txt
    ```

2. Ver última línea e analizar a estructura

    ```bash
    tail -n 1 ficheiros/access_log.txt
    ```

3. Ver `IPs` de clientes

    ```bash
    awk '{ print $1 }' ficheiros/access_log.txt | sort -u
    ```

4. Ver `IPs` de clientes e o nº de peticións que hacen ordenadas de maior a menor

    ```bash
    awk '{ print $1 }' ficheiros/access_log.txt | sort | uniq -c | sort -nr
    ```

5. Ver códigos de respuesta

    ```bash
    awk -F '"' '{ print $3 }' ficheiros/access_log.txt | awk '{ print $1 }' | sort -un

    # Con número repeticións
    awk -F '"' '{ print $3 }' ficheiros/access_log.txt | awk '{ print $1 }' | sort | uniq -c | sort -nr
    ```

6. Ver peticións do equipo más activo

    ```bash
    grep $( awk '{ print $1 }' ficheiros/access_log.txt | sort | uniq -c | sort -nr | head -n 1 | awk '{ print $2 }') ficheiros/access_log.txt

    # Con número repeticións
    grep $( awk '{ print $1 }' ficheiros/access_log.txt | sort | uniq -c | sort -nr | head -n 1 | awk '{ print $2 }' ) ficheiros/access_log.txt | awk -F '"' '{ print $2 }' | sort | uniq -c | sort -nr

    # Con número repeticións (mellor)
    grep $( awk '{ print $1 }' ficheiros/access_log.txt | sort | uniq -c | sort -nr | head -n 1 | awk '{ print $2 }') ficheiros/access_log.txt | sort | uniq -c | sort -nr

    # Só peticións sen repeticións
    grep $( awk '{ print $1 }' ficheiros/access_log.txt | sort | uniq -c | sort -nr | head -n 1 | awk '{ print $2 }') ficheiros/access_log.txt | sort -u

    # Só peticións sen repeticións (mellor)
    grep $( awk '{ print $1 }' ficheiros/access_log.txt | sort | uniq -c | sort -nr | head -n 1 | awk '{ print $2 }' ) ficheiros/access_log.txt | awk -F '"' '{ print $2 }' | sort -u
    ```

7. Ver user-agent e cantas peticións se ejecutan con cada uno de ellos

    ```bash
    awk -F '"' '{ print $6 }' ficheiros/access_log.txt | sort | uniq -c | sort -nr
    ```

8. Buscar as peticións realizadas por o `User-Agent` `Teh Forest Lobster`

    ```bash
    grep 'Teh Forest Lobster' ficheiros/access_log.txt | sort | uniq -c | sort -nr
    ```

9. Ver peticións onde apareza a palabra `admin`

    ```bash
    grep -i 'admin' ficheiros/access_log.txt | sort | uniq -c | sort -nr
    ```

10. Buscar a `IP` dos solicitantes entre as peticións onde apareza a palabra `admin`

    ```bash
    grep -i 'admin' ficheiros/access_log.txt | awk '{ print $1 }' | uniq -c | sort -nr
    ```

11. Buscar entre as peticións onde apareza a palabra `admin`, los códigos de respuesta para ver se algunha solicitude tivo éxito

    ```bash
    grep -i 'admin' ficheiros/access_log.txt | awk '$9 == 200' | sort | uniq -c | sort -nr

    # Alternativa (problema se hai outro campo co '200')
    grep -i 'admin' ficheiros/access_log.txt | grep -w '200'
    ```

12. Localizar a línea onde a solicitude `admin` obtivo éxito e mostrar 2 liñas anteriores e posteriores
    - con grep/awk e sed en dos comandos

    ```bash
    grep -n -i 'admin' ficheiros/access_log.txt | awk '$9 == 200' # dá a liña 1026
    sed -n 1024,1028p ficheiros/access_log.txt
    ```

    - con awk e grep

    ```bash
    grep -A 2 -B 3 -i 'admin http/1.1" 200' ficheiros/access_log.txt  # -A despois -B antes
    grep -C 2 -i 'admin http/1.1" 200' ficheiros/access_log.txt # -C antes e despois

    # Con sed
    sed '/admin http\/1.1" 200/p' ficheiros/access_log.txt
    ```

13. Búsquedas peticións onde apareza a palabra `dallas`

    ```bash
    awk -F '"' '$2 ~ /dallas/ {print $2}' ficheiros/access_log.txt

    # Quitando duplicados
    awk -F '"' '$2 ~ /dallas/ {print $2}' ficheiros/access_log.txt | sort -u
    awk -F '"' '$2 ~ /dallas/ {print $2}' ficheiros/access_log.txt | sort | uniq -c | sort -nr
    ```

## ENISA 1: ficheiro 24022007.txt

1. Nº de líneas

    ```bash
    wc -l ficheiros/24022007.txt
    ```

2. Ver as primeras liñas para facernos unha idea de a estructura de as mismas

    ```bash
    head -10 ficheiros/24022007.txt
    ```

3. Crear unha lista de `IPs` de posibles atacantes ordenándolas en base al número de aparicións e despois mostrar o nº de aparicións e o nº total de equipos

    ```bash
    grep -i 'udp' ficheiros/24022007.txt | awk '{print $5}' | awk -F ':' '{print $1}' | sort | uniq -c | sort -nr
    ```

> Generate a list of unique attacking IP addresses. How many distinct source hosts were taking part in the attack? (Assume that attacking packets = UDP packets). Descartando as `IPs` privadas de clase A:
